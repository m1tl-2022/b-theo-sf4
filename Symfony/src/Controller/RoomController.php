<?php

namespace App\Controller;

use App\Entity\Room;
use App\Form\RoomType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\RoomRepository;
use DateTime;
use DateTimeZone;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class RoomController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/room", name="room")
     */
    public function index(RoomRepository $RoomRepo): Response
    {

        $rooms = $RoomRepo->findAvailableRoom();

        return $this->render('room/index.html.twig', [
            'controller_name' => 'RoomController',
            'rooms' => $rooms
        ]);
    }

    /**
     * @Route("/room/new", name="room.add", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function new(Request $request): Response
    {
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('Europe/Paris'));

        $room = new Room();
        $room->setUser($this->security->getUser());
        $room->setCreatedAt($date);

        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($room);
            $entityManager->flush();

            $this->addFlash('success', 'La salle a bien été ajoutée');

            return $this->redirectToRoute('room');
        }

        return $this->render('room/new.html.twig', [
            'room' => $room,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/room/{id}/edit", name="room.edit", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function edit(Request $request, Room $room): Response
    {
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('Europe/Paris'));

        $room->setUpdatedAt($date);

        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'La modification de la salle a bien été enregistrée');

            return $this->redirectToRoute('room');
        }

        return $this->render('room/edit.html.twig', [
            'room' => $room,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/room/{id}", name="room.delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function delete(Request $request, Room $room): Response
    {
        if ($this->isCsrfTokenValid('delete' . $room->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($room);
            $entityManager->flush();

            $this->addFlash('success', 'La salle a bien été supprimée');
        }
        return $this->redirectToRoute('room');
    }
}
