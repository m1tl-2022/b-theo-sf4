<?php

namespace App\Form;

use App\Entity\Room;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RoomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'Ex : Salle n°5',
                    'class' => 'my-2'
                ]
            ])
            ->add('capacity', IntegerType::class,  [
                'attr' => [
                    'class' => 'my-2'
                ]
            ])
            ->add('city', TextType::class, [
                'attr' => [
                    'class' => 'my-2'
                ]
            ])
            ->add('description', TextareaType::class, [
                'attr' => [
                    'class' => 'my-2'
                ], 
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci d\'ajouter une description'
                    ]),
                    new Length([
                        'min' => 10,
                        'max' => 600,
                        'minMessage' => 'Votre description doit contenir au moins 10 caractères',
                        'maxMessage' => 'Votre description doit contenir moins de 600 caractères'
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Room::class,
            'translation_domain' => 'room_form'
        ]);
    }
}
